
/*--
 * Definición de la clase Contacto con sus atributos u métodos.
 * 
 */
public class Contacto {
    private String nombre;
    private String apellido;
    private String telefono;
  
 //---------------------  CONSTR
    public Contacto()  
    {
    this.nombre=null;
    this.telefono=null;
    this.apellido=null;
    }
    public Contacto(String pNombre,String pApellido, String pTelefono) {
        this.nombre = pNombre;
        this.telefono = pTelefono;
        this.apellido=pApellido;
    }
    
 //-------------------  getter & setters
    
    public void setNombre(String pNombre){        
        this.nombre=pNombre;
    }
    public void setTelefono(String pTelf){
        this.telefono=pTelf;
    }
 
    public String getNombre() {
        return this.nombre;
    }
 
    public String getTelefono() {
        return this.telefono;
    }
	@Override
	public String toString() {
		return "Contacto [nombre=" + nombre + ", apellido=" + apellido + ", telefono=" + telefono + "]";
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String pApellido) {
		this.apellido = pApellido;
	}      
    
}