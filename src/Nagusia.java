import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * azterketa programa nagusia 2017-11-22 EXAMEN PROGRAMACI�N, programa principal
 * 
 */
public class Nagusia {

	public static void main(String[] args) {

		// -------------Aldagaiak
		Agenda agenda = new Agenda();

		// -------------Programa
		Scanner sc = new Scanner(System.in);
		int opcion = 1;

		load_data(agenda); // fitxero batetik kargatzen dira datuak / se cargan los datos de un fichero

		while (opcion != 0) {
			
			ver_menu();
			
			opcion = sc.nextInt();
			sc.nextLine();

			switch (opcion) {
			case 1:
				agenda.altaContacto();
				break;
			case 2:			
				agenda.buscarContacto();
				break;
			case 3:
				agenda.modificarContacto();
				break;
			case 4:
				agenda.borrarContacto();
				break;
			case 5:			
				agenda.toString();
				break;
			case 0:
				System.out.println("AGUR!!");
				break;
			default:
				System.out.println("Opci�n incorrecta ...");
			}
		}
		sc.close();
	}

	private static void ver_menu() {
		System.out.println("......................");
		System.out.println("AGENDA");
		System.out.println("......................");
		System.out.println("1-Nuevo contacto");
		System.out.println("2-Buscar contacto");
		System.out.println("3-Modificar numero a un contacto");
		System.out.println("4-Eliminar un contacto");
		System.out.println("5-Listado de contactos");
		System.out.println("0-Salir");
		System.out.println("......................");
		System.out.println("Opci�n: ");
		System.out.println("......................");
		
	}

	public static void load_data(Agenda agenda) {
		String line, nombre, apellido, telefono;
		String[] split_line;
		try {
			File fich = new File("src/datuak/fitxategia.txt");
			Scanner sc = new Scanner(fich);

			while (sc.hasNextLine()) {
				line = sc.nextLine();
				split_line = line.split("::");
				nombre = split_line[0];
				apellido = split_line[1];
				telefono = split_line[2];
				Contacto nuevo = new Contacto(nombre, apellido, telefono);
				agenda.aniadirContacto(nuevo);
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("El archivo no existe...");
		}
	}
}